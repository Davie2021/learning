<?php
/***
Plugin Name: Last Mile Program
Plugin URI: https://kanzucode.com
Description: Custom plugin for learning purposes
Author: Davies, Stephen & Elizabeth
Version: 1.0.0
*/


 add_action ('init', 'create_post');

function create_post(){
    $post = array(
        'post_title' => 'Hello Post',
        'post_content' => 'Post content',
        'post_author' => 1,
        'post_type' => 'post'
    );
    wp_insert_post ($post);
}

// changes made
add_action( 'draft_post', 'create_post');

function create_post(){
    $post = array(
        'post_title' => 'Hello Post',
        'post_content' => 'Post content',
        'post_status' => 'publish',
        'post_author' => get_current_user_id(),
        'post_type' => 'post',
    ); 
    wp_insert_post($post);
}
add_action('init', 'add_comment');
function add_comment(){
    $comment = array(
        'comment_post_ID' => get_current_user_id(),
        'comment_author' => 'Dave',
        'comment_author_email' => 'dave@domain.com',
        'comment_author_url' => 'http://www.someiste.com',
        'comment_content' => 'Hello comment',
        'comment_author_IP' => '127.3.1.1',
        'comment_date' => date('Y-m-d H:i:s'),
        'comment_date_gmt' => date('Y-m-d H:i:s'),
        'comment_approved' => 1,
    );
    wp_insert_comment($comment);

}
